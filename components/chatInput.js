import {useState} from "react";

function ChatInput({ onSubmit }) {
    const [userInput, setUserInput] = useState('');

    const submitMessage = (event) => {
        setUserInput('');
        onSubmit(event);
    }

    return (
        <form onSubmit={submitMessage}>
            <input
                type="text"
                name="chat"
                placeholder="Enter message"
                value={userInput}
                onChange={(e) => setUserInput(e.target.value)}
            />
            <input type="submit" value="Send" />
        </form>
    )
}

export default ChatInput;