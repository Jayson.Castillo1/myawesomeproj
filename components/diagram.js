import ReactFlow, {Controls, Background, useNodesState, useEdgesState, addEdge} from 'reactflow';
import 'reactflow/dist/style.css';
import {useCallback, useRef, useState} from "react";

function convertToReactFlowJson(inputJson) {
  const nodes = [];
  const edges = [];

  const nodePositions = {
    x: 50,
    y: 50,
  };

  const edgePositionOffset = 150;

  inputJson.steps.forEach((step, index) => {
    const nodeId = `node-${step.step}`;
    const node = {
      id: nodeId,
      //type: step.type === 'finished' ? 'output' : 'input',
      data: {
        label: step.type === 'finished' || step.type === 'transfer' ? capitalizeFirstLetter(step.type) : step.prompt || step.activity,
        resourceUrl: step.resourceUrl,
        entity: step.entity,
        variable: step.variable,
        items: step.items,
        condition: step.condition,
      },
      position: {
        x: nodePositions.x + index * edgePositionOffset,
        y: nodePositions.y + index * edgePositionOffset,
      },
      draggable: true,
    };

    nodes.push(node);

    if (step.transitions) {
      step.transitions.forEach((transition) => {
        const edge = {
          id: `edge-${nodeId}-${transition.next}`,
          source: nodeId,
          target: `node-${transition.next}`,
          label: transition.condition || '',
        };
        edges.push(edge);
      });
    }
  });

  return { nodes, edges };
}

function capitalizeFirstLetter(string) {
  return string[0].toUpperCase() + string.slice(1);
}

function Diagram({ data }) {
  const model = convertToReactFlowJson(data);
  const reactFlowWrapper = useRef(null);
  const [nodes, setNodes, onNodesChange] = useNodesState(model.nodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState([]);
  const [reactFlowInstance, setReactFlowInstance] = useState(null);

  const onDragOver = useCallback((event) => {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }, []);

  const onDrop = useCallback(
    (event) => {
      event.preventDefault();

      const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
      const type = event.dataTransfer.getData('application/reactflow');

      // check if the dropped element is valid
      if (typeof type === 'undefined' || !type) {
        return;
      }

      const position = reactFlowInstance.project({
        x: event.clientX - reactFlowBounds.left,
        y: event.clientY - reactFlowBounds.top,
      });
      const newNode = {
        id: getId(),
        type,
        position,
        data: { label: `${type} node` },
      };

      setNodes((nds) => nds.concat(newNode));
    },
    [reactFlowInstance]
  );

  return (
    <div style={{ height: '800px', width: '100%' }}>
      <ReactFlow nodes={nodes} edges={model.edges} onNodesChange={onNodesChange}
                 onEdgesChange={onEdgesChange}
                 onInit={setReactFlowInstance}
                 onDrop={onDrop}
                 onDragOver={onDragOver}
                 fitView>
        <Background />
        <Controls />
      </ReactFlow>
    </div>
  );
}

export default Diagram;
