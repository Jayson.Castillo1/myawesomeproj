import Head from "next/head";
import {useEffect, useRef, useState} from "react";
import styles from "./index.module.css";
import {Spinner} from "react-bootstrap";
import Diagram from "../components/diagram";
import ChatInput from "../components/chatInput";
import objectHash from "object-hash";

export default function Home() {
  const [initialPrompt, setInitialPrompt] = useState('');
  const [messages, setMessages] = useState([]);
  const [authToken, setAuthToken] = useState('');
  const [loading, setLoading] = useState(false);
  const [mostRecentGPTMessage, setMostRecentGPTMessage] = useState(null);
  const [jsonError, setJsonError] = useState(false);
  const [displayError, setDisplayError] = useState(false);
  const chatAreaEnd = useRef(null);

  const scrollToBottom = () => {
    chatAreaEnd?.current?.scrollIntoView({ behavior: 'smooth' });
  };

  useEffect(() => {
    scrollToBottom();
  }, [JSON.stringify(messages)]);

  async function onSubmit(event) {
    event.preventDefault();
    let input = event.target[0].value;

    let rawChatMessage = input;
    if (!initialPrompt) {
      rawChatMessage = createPromptForTopic(rawChatMessage);
      setInitialPrompt(rawChatMessage);
    }

    await sendChatMessageToGPT(input, rawChatMessage);
  }

  async function sendChatMessageToGPT(displayMessage, rawChatMessage) {
    const allMessages = [...messages, {displayMessage, role: 'user', content: rawChatMessage}];
    setMessages(allMessages);
    setLoading(true);
    const apiPayload = allMessages.map((message) => {
      return {
        role: message.role,
        content: message.content
      }
    });

    try {
      const response = await fetch("/api/generate", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ messages: apiPayload }),
      });

      const data = await response.json();
      if (response.status !== 200) {
        throw data.error || new Error(`Request failed with status ${response.status}`);
      }

      console.log(data.result);
      allMessages.push({ role: 'assistant', content: data.result });
      setMessages(allMessages);
      setMostRecentGPTMessage(data.result);
      setLoading(false);
      setJsonError(false);
    } catch(error) {
      // Consider implementing your own error handling logic here
      console.error(error);
      alert(error.message);
    }
  }

  async function importGPTResponse() {
    if (!authToken) {
      await fetch("https://api.int2.us.five9nonprod.net/cloudauthsvcs/v1/service2service/token", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Basic bDJ2NEkxc3EwT1RHRjFZUmMxeDRhQk1wY3VmWDdUWFc6WlNJbG1UMFY3cm5Id2tpag=="
        },
        body: JSON.stringify({ grant_type: "client_credentials" }),
      })
          .then((response) => response.json())
          .then(async (response) => {
            console.log(response);
            setAuthToken(response.access_token);
            await postToSunshineProvider(response.access_token);
          });
    } else {
      await postToSunshineProvider();
    }
  }

  async function postToSunshineProvider(accessToken) {
    const response = await fetch("https://sec-1.int2.us.five9nonprod.net/preview-app/feature-hackathon/sunshine-provider/v1/domains/1/topics", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: mostRecentGPTMessage
    }).catch((error) => {
      console.error(error);
      alert(error.message);
    });

    if (response.status === 204) {
      alert('Import Successful');
    }
  }

  function createPromptForTopic(topic) {
    return `You are a helpful assistant. You can help with generating topics using JSON as response. I want to build an ${topic} topic. Types include question, message, finished and transfer. Entities include text, list and number.  Reply with JSON only.
Example response:  
{
   "name":"User Info Collection",
   "steps":[
      {
         "step":"1",
         "type":"question",
         "prompt":"What is your name?",
         "entity":"text",
         "variable":"UserInfo.Name",
         "resourceUrl":"https://cdn.dribbble.com/users/291221/screenshots/1425333/helper.gif",
         "transitions":[
            {
               "next":"2"
            }
         ]
      },
      {
         "step":"2",
         "type":"question",
         "prompt":"What is your email address?",
         "entity":"text",
         "variable":"UserInfo.Email",
         "transitions":[
            {
               "next":"3"
            }
         ]
      },
      {
         "step":"3",
         "type":"question",
         "prompt":"What is your gender?",
         "entity":"list",
         "items":[
            {
               "id":"M",
               "displayName":"Male"
            },
            {
               "id":"F",
               "displayName":"Female"
            }
         ],
         "variable":"UserInfo.Gender",
         "transitions":[
            {
               "next":"4",
               "condition":"UserInfo.Gender = 'M'"
            },
            {
               "next":"5",
               "condition":"UserInfo.Gender = 'F'"
            }
         ]
      },
      {
         "step":"4",
         "type":"message",
         "activity":"Thank you for providing your information, Sir, {UserInfo.Name}. Your email address is {UserInfo.Email}. We have your details recorded for future references.",
         "transitions":[
            {
               "next":"6"
            }
         ]
      },
      {
         "step":"5",
         "type":"message",
         "activity":"Thank you for providing your information, Madam, {UserInfo.Name}. Your email address is {UserInfo.Email}. We have your details recorded for future references.",
         "transitions":[
            {
               "next":"6"
            }
         ]
      },
      {
         "step":"6",
         "type":"finished"
      }
   ]
}`;
  }

  function createFixJsonPrompt() {
    return `Can you please try to correct the syntax error in the following JSON and reply with JSON only: ${mostRecentGPTMessage}`;
  }

  function generateDiagram() {
    let json;
    try {
      json = JSON.parse(mostRecentGPTMessage);
    } catch (error) {
      setJsonError(true);
      setDisplayError(true);
      console.error('Error parsing GPT\'s JSON: ' + error);
    }

    if (json) {
      const key = objectHash(json);
      return (
          <div id={'diagram'} style={{ width: '50%', flex: 1, marginRight: "10px" }}>
            <Diagram data={json} key={key} />
            <div>
              <button
                  style={{
                    margin: "10px auto",
                    padding: "12px",
                    color: "#fff",
                    background: "linear-gradient(#3396C6, #2281B8)",
                    border: "none",
                    borderRadius: "4px",
                    textAlign: "center",
                    cursor: "pointer",
                    width: "100%"
                  }}
                  onClick={importGPTResponse}
              >
                Import
              </button>
            </div>
          </div>
      );
    }
  }

  return (
    <div>
      <Head>
        <title>Five9 Hackathon Team 4</title>
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css"
            integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM"
            crossOrigin="anonymous"
        />
      </Head>

      <div style={{ display: 'flex', width: '100%' }}>
        <main className={styles.main} style={{ width: '50%', flex: 1 }} >
          <div style={{
            width: "700px",
            margin: "10px auto",
            padding: "10px",
            border: "1px solid #3396C6",
            borderRadius: "10px",
            maxHeight: "700px",
          }}>
            <div style={{
              color: "white",
              backgroundColor: "black",
              borderRadius: "10px",
            }}>
              <p style={{padding: "10px"}}>Enter your topic to start the conversation</p>
            </div>
            <div style={{maxHeight: "480px", height: "480px", overflow: "auto", padding: "10px"}}>
              {messages.map((message, index) => {
                let displayMessage = message.content;
                const isUserMessage = message.role === 'user';
                if (isUserMessage) {
                  displayMessage = message.displayMessage;
                } else {
                  displayMessage = (
                    <textarea
                      style={{minWidth: '460px'}}
                      disabled={false}
                      value={displayMessage}
                    />
                  );
                }
                const bubbleColor = isUserMessage ? "#3396C6" : "#DDDDDD";
                return (
                  <div key={index}>
                    <div
                      style={{
                        width: "fit-content",
                        float: isUserMessage ? "right" : "left",
                        height: "fit-content",
                        maxWidth: "500px"
                      }}>
                      <p style={{
                        backgroundColor: `${bubbleColor}`,
                        border: `1px solid ${bubbleColor}`,
                        borderRadius: "15px",
                        padding: "5px 10px",
                        color: "white"
                      }}>
                        {displayMessage}
                      </p>
                    </div>
                    <br style={{clear: "both"}} />
                  </div>
                );
              })}
              {loading && (
                <Spinner animation="border" role="status" size={"sm"} style={{color: "#3396C6"}}>
                  <span className="visually-hidden">Loading...</span>
                </Spinner>
              )}
              {jsonError && displayError && (
                  <div
                      style={{
                        color: "white",
                        backgroundColor: "red",
                        borderRadius: "10px",
                      }}
                      onClick={async () => {
                        setDisplayError(false);
                        await sendChatMessageToGPT('Please fix the error in your previous response.', createFixJsonPrompt())
                      }}
                  >
                    <p style={{padding: "10px", cursor: "pointer"}}>A syntax error occurred while parsing Chat GPT's JSON. Click here to try to correct it.</p>
                  </div>
              )}
              <div ref={chatAreaEnd} />
            </div>
            <ChatInput onSubmit={onSubmit} />
          </div>
        </main>
        {mostRecentGPTMessage && !jsonError && generateDiagram()}
      </div>
    </div>
  );
}
